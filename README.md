#php-multiple-file-uploader

Copyright Kris Occhipinti 2021-09-20

(https://filmsbykris.com)

License GPLv3

```
mkdir uploads
chmod 755 uploads
```

set php to allow larger uploads
```
sudo vim /etc/php/7.x/apache2/php.ini
```
change the following:

```
upload_max_filesize = 10M
post_max_size = 10M
max_file_uploads = 25
```
