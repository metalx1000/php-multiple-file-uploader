<!DOCTYPE html>
<html lang="en">
  <head>
    <title></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style>
      html *{
        font-size: 35px !important;
        margin: 2px;
      }

      @media only screen and (min-width: 600px) {
        body {
          margin-right:200px;
          margin-left:200px;
          margin-top: 0;
        }
      }
      .flex{
        display: grid;
        grid-row-gap: 1rem;
        grid-template-columns: repeat(auto-fit, minmax(150px, 1fr));
      }

    </style>
    <script>
      document.addEventListener("DOMContentLoaded", function(){

      });
      // Upload file
      function uploadFile() {

        var totalfiles = document.getElementById('files').files.length;

        if(totalfiles > 0 ){

          var formData = new FormData();

          // Read selected files
          for (var index = 0; index < totalfiles; index++) {
            formData.append("files[]", document.getElementById('files').files[index]);
          }

          var xhttp = new XMLHttpRequest();

          // Set POST method and ajax file path
          xhttp.open("POST", "upload.php", true);

          // call on request changes state
          xhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {

              var response = this.responseText;

              alert(response + " File uploaded.");

            }
          };

          // Send request with data
          xhttp.send(formData);

          }else{
          alert("Please select a file");
        }

      }
    </script>
  </head>
  <body>
    <div >
      <input type="file" name="files" id="files" multiple>
      <input type="button" id="btn_uploadfile" 
      value="Upload" 
      onclick="uploadFile();" >
    </div>
  </body>
</html>
